package com.devdavi.provapratica.adapter

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.devdavi.provapratica.Interface.ItemClickListener
import com.devdavi.provapratica.R
import com.devdavi.provapratica.model.RSSObject
import java.lang.Exception
import java.text.SimpleDateFormat

class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener,
    View.OnLongClickListener {
    val txtTitle: TextView = itemView.findViewById(R.id.txtTitle)
    val txtPubDate: TextView = itemView.findViewById(R.id.txtpubDate)
    val txtDescription: TextView = itemView.findViewById(R.id.txtDescription)
    val imageView: ImageView = itemView.findViewById(R.id.imageView)

    private var itemClickListener: ItemClickListener? = null

    init {
        itemView.setOnClickListener(this)
        itemView.setOnLongClickListener(this)
    }

    fun setItemClickListener(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    override fun onClick(view: View?) {
        itemClickListener!!.onClick(view, adapterPosition, false)
    }

    override fun onLongClick(view: View?): Boolean {
        itemClickListener!!.onClick(view, adapterPosition, false)
        return true
    }

}

class FeedAdapter(private val rssObject: RSSObject, private val context: Context) :
    RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView =
            LayoutInflater.from(context).inflate(R.layout.adapter_feed, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val content = rssObject.items!![position].content_html
        val positionInit = content!!.indexOf("<br />   ") + 9
        val description = content.substring(positionInit, (positionInit + 30)) + "..."
        try {
            val df = SimpleDateFormat("dd/MM/yyyy")
            val pubDate = df.format(rssObject.items!![position].date_published)
            holder.txtPubDate.text = pubDate

        } catch (e: Exception) {
            Log.e("ErrorParse", "Error while parse data")
        }

        holder.txtTitle.text = rssObject.items!![position].title!!
        holder.txtDescription.text = description

        val inicioUrl = content.indexOf("https:")
        val finalUrl = content.indexOf(" /><br />") - 1
        if (inicioUrl != -1 && finalUrl != -1) {
            val urlImage =
                content.substring(inicioUrl, finalUrl)
            val uri = Uri.parse(urlImage)
            Glide.with(context).load(uri).into(holder.imageView)
        }

        holder.setItemClickListener { view, position, isLongClick ->
            if (!isLongClick) {
                val browserIntent =
                    Intent(Intent.ACTION_VIEW, Uri.parse(rssObject.items!![position].url))
                browserIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                context.startActivity(browserIntent)
            }
        }
    }

    override fun getItemCount(): Int {
        return rssObject.items!!.size
    }
}