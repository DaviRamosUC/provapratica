package com.devdavi.provapratica.activities

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.devdavi.provapratica.adapter.FeedAdapter
import com.devdavi.provapratica.databinding.ActivityMainBinding
import com.devdavi.provapratica.model.RSSObject
import com.google.gson.Gson
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    private val RSS_link = "https://g1.globo.com/rss/g1/carros/"
    private val RSS_to_JSON_API = "https://www.toptal.com/developers/feed2json/convert?url="

    private lateinit var binding: ActivityMainBinding
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: FeedAdapter
    private lateinit var rssObject: RSSObject
    private lateinit var mQueue: RequestQueue

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mQueue = Volley.newRequestQueue(applicationContext)
        loadRSS()

        //Initiate widget's
        recyclerView = binding.recyclerView

    }

    private fun loadRSS() {
        val builder = StringBuilder(RSS_to_JSON_API)
        builder.append(RSS_link)
        val url: String = builder.toString()

        val request: JsonObjectRequest = JsonObjectRequest(
            Request.Method.GET,
            url,
            null,
            object :
                Response.Listener<JSONObject>, Response.ErrorListener {
                override fun onResponse(response: JSONObject?) {
                    rssObject =
                        Gson().fromJson(response.toString(), RSSObject::class.java)
                    // Adapter Configuration
                    adapter = FeedAdapter(rssObject, applicationContext)

                    //RecyclerView Configuration
                    val layoutManager = LinearLayoutManager(applicationContext)
                    recyclerView.layoutManager = layoutManager
                    recyclerView.setHasFixedSize(false)
                    recyclerView.adapter = adapter
                }

                override fun onErrorResponse(error: VolleyError?) {
                    Toast.makeText(
                        applicationContext,
                        "Ocorreu um erro durante o carregamento",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        ) {
            Toast.makeText(
                applicationContext,
                "Ocorreu um erro durante o carregamento",
                Toast.LENGTH_LONG
            ).show()
        }

        mQueue.add(request)
    }
}