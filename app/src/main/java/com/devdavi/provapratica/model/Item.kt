package com.devdavi.provapratica.model

import java.util.*

data class Item(
    var guid: String? = null,
    var url: String? = null,
    var title: String? = null,
    var content_html: String? = null,
    var summary: String? = null,
    var date_published: Date? = null
)
