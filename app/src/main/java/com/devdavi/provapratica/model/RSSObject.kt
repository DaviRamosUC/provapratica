package com.devdavi.provapratica.model

data class RSSObject(
    var version: String? = null,
    var title: String? = null,
    var home_page_url: String? = null,
    var description: String? = null,
    var items: List<Item>? = null
)
